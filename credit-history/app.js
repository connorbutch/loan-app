const tableName = process.env.TABLE_NAME;
const queueUrl = process.env.QUEUE_URL;

const AWSXRay = require('aws-xray-sdk');
AWSXRay.setContextMissingStrategy(() => {
});

const AWS = AWSXRay.captureAWS(require("aws-sdk"));
AWS.config.update({region: process.env.AWS_REGION})
const dynamoDB = new AWS.DynamoDB.DocumentClient(); //NOTE: this client is automatically captured via the captureaws line above
const sqs = new AWS.SQS(); //NOTE: this client is automatically captured via the captureaws line above

exports.handler = async (event, context) => {
    return await AWSXRay.captureAsyncFunc('handleRequest',
        async function () {
            const ssns = AWSXRay.captureFunc("parseSsns", function () {
                return parseSsnsFromEvent(event);
            });
            const loanApplicationIds = AWSXRay.captureFunc("parseLoanApplicationIds", function () {
                return getLoanApplicationIdsFromEvent(event)
            });
            AWSXRay.getSegment().addAnnotation("loanApplicationIds", loanApplicationIds.join());
            const creditHistoryForSsn = await AWSXRay.captureAsyncFunc("getCreditHistoryForSsns", async function (subsegment) {
                const value = await getCreditHistoryForSsns(ssns);
                subsegment.close();
                return value;
            });
            await AWSXRay.captureAsyncFunc("publishLoanRequestWithCreditHistory", async function (subsegment) {
                const value = await publishLoanRequestWithCreditHistory(creditHistoryForSsn, event.Records);
                subsegment.close();
                return value;
            });
            AWSXRay.getSegment().close();
            return {};
        });
}

function parseSsnsFromEvent(event) {
    return event.Records.map(record => record.body)
        .map(record => JSON.parse(record))
        .map(recordObject => recordObject.ssn);
}

function getLoanApplicationIdsFromEvent(event) {
    return event.Records.map(record => record.body)
        .map(record => JSON.parse(record))
        .map(recordObject => recordObject.loanApplicationId);
}

async function getCreditHistoryForSsns(ssns) {
    const params = buildBatchGetParams(ssns);
    AWSXRay.getSegment().addMetadata("batchGetParams", params, "dynamo")
    const batchGetResult = await dynamoDB
        .batchGet(params)
        .promise();
    AWSXRay.getSegment().addMetadata("batchGetResult", batchGetResult, "dynamo")
    //NOTE: in the real world, you would have to check for unprocessed keys here as well
    return parseCreditHistoryIntoSsnKeyedObject(batchGetResult, ssns);
}

async function publishLoanRequestWithCreditHistory(creditHistoryForSsn, records) {
    const entries = records.map(record => {
        return record.body
    })
        .map(record => {
            return JSON.parse(record);
        })
        .map(recordObject => {
            recordObject.creditScore = creditHistoryForSsn[recordObject.ssn];
            return recordObject;
        })
        .map(recordObject => {
            return {
                Id: Date.now().toString(),  //NOTE: in the real world, you would not want to use a random value here, but rather something you can tie back to the record you send
                //this id is what is used to determine which records passed and failed
                MessageBody: JSON.stringify(recordObject)
            }
        })
    const params = {
        QueueUrl: queueUrl,
        Entries: entries
    };
    AWSXRay.getSegment().addMetadata("batchSendMessageRequest", params, "sqs")
    const sendMessageBatchResult = await sqs.sendMessageBatch(params).promise();
    AWSXRay.getSegment().addMetadata("batchSendMessageResult", sendMessageBatchResult, "sqs")
    if (!sendMessageBatchResult.Failed) {
        //TODO here is where you would handle failed records based on the id -- this is why the id should really be tied to the input
    }
    return sendMessageBatchResult;
}

function buildBatchGetParams(ssns) {
    ssns = removeDuplicates(ssns);
    const params = {
        RequestItems: {},
    };
    const ssnKeys = ssns.map(ssn => {
        return {
            id: ssn
        };
    })
    params.RequestItems[tableName] = {
        Keys: ssnKeys,
    }
    return params;
}

function parseCreditHistoryIntoSsnKeyedObject(batchGetResult, ssns) {
    const tableSpecificResults = batchGetResult.Responses[tableName];
    const returnValue = {};
    ssns.forEach(ssn => {
        const result = tableSpecificResults.find((e) => e.id === ssn);
        const creditScoreForSsn = result === undefined ? 0 : result.creditScore;
        returnValue[ssn] = creditScoreForSsn;
    });
    return returnValue;
}

function removeDuplicates(arr) {
    return [...new Set(arr)];
}