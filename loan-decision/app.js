const {SNSClient,PublishBatchCommand} = require("@aws-sdk/client-sns");
const AWSXRay = require('aws-xray-sdk');
AWSXRay.setContextMissingStrategy(() => {
});
const minimumCreditScore = 700;

const topicArn = process.env.TOPIC_ARN;
const snsClient = AWSXRay.captureAWSv3Client(new SNSClient({region: process.env.AWS_REGION}));

exports.handler = async (event, context) => {
    return await AWSXRay.captureAsyncFunc('handleRequest',
        async function () {
            AWSXRay.getSegment().addMetadata("event", event, "input");
            console.log(event);
            const approvedLoans = [];
            const rejectedLoans = [];
            for (const record of event.Records) {
                if(await isApproved(record)){
                    AWSXRay.getSegment().addAnnotation("isApproved", true);
                    approvedLoans.push(record);
                }else{
                    AWSXRay.getSegment().addAnnotation("isApproved", false);
                    rejectedLoans.push(record);
                }
            }
            //you can only send 10 records at once to sns, so I'm not worried about performance of another loop here so we can have enhanced readability
            await publishApprovedAndRejectedLoanEvents(approvedLoans, rejectedLoans);
            AWSXRay.getSegment().close();
            return {};
        })
};

async function isApproved(record) {
    return AWSXRay.captureAsyncFunc('approvalDecision', async function () {
        const bodyObject = JSON.parse(record.body);
        if(isSlowDecision(bodyObject.ssn)){
            console.log("Making this a slow decision on purpose to show x-ray segment size usage");
           const dummyValue = await sleep(7000);
           console.log(dummyValue);
        }
        const loanApplicationId = bodyObject.loanApplicationId;
        AWSXRay.getSegment().addAnnotation("loanApplicationId", loanApplicationId);
        const creditScore = bodyObject.creditScore;
        AWSXRay.getSegment().addMetadata("customerCreditScore", creditScore, "approvalDecision");
        const isAccepted = bodyObject.creditScore > minimumCreditScore;
        if(!isAccepted){
            const rejectionReason = `Customer credit score of ${creditScore} is less than required credit score of ${minimumCreditScore}`
            AWSXRay.getSegment().addMetadata("rejectionReason", rejectionReason, "approvalDecision");
        }
        AWSXRay.getSegment().close();
        return isAccepted;
    });
}

function buildBatchPublishCommand(approvedLoans, rejectedLoans) {
    let publishBatchRequestEntries = approvedLoans.map(approvedLoan => {
        return {
            Id: Date.now().toString(), //NOTE: In the real world, you would want to be able to tie the id back to the message, so use a map if you use a random number for it
            Message: JSON.stringify(approvedLoan),
            MessageAttributes: {
                "TYPE": {
                    "DataType": "String",
                    "StringValue": "LOAN_APPROVED",
                    "BinaryValue": null
                }
            }
        };
    });
    const rejectedBatchEntries = rejectedLoans.map(rejectedLoan => {
        return {
            Id: Date.now().toString(), //NOTE: In the real world, you would want to be able to tie the id back to the message, so use a map if you use a random number for it
            Message: JSON.stringify(rejectedLoan),
            MessageAttributes: {
                "TYPE": {
                    "DataType": "String",
                    "StringValue": "LOAN_REJECTED",
                    "BinaryValue": null
                }
            }
        };
    });
    publishBatchRequestEntries = publishBatchRequestEntries.concat(rejectedBatchEntries);
    return new PublishBatchCommand({
        TopicArn: topicArn,
        PublishBatchRequestEntries: publishBatchRequestEntries
    });
}

async function publishApprovedAndRejectedLoanEvents(approvedLoans, rejectedLoans) {
    return await AWSXRay.captureAsyncFunc('publishToSns',
        async function (subsegment) {
            const publishBatchCommand = buildBatchPublishCommand(approvedLoans, rejectedLoans);
            //TODO add metadata for request
            subsegment.addMetadata("publishBatchCommand", publishBatchCommand, "sns");
            const publishBatchResult = await snsClient.send(publishBatchCommand);
            if(publishBatchResult.Failed.length !== 0){
                //NOTE: this is where you would handle failures in a real world app
            }
            subsegment.addMetadata("publishBatchOutput", publishBatchResult, "sns");
            console.log("publish batch result: ", publishBatchResult);
            subsegment.close();
            return publishBatchResult; //TODO what do we want to return
        });
}

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
        return 4;
    });
}

function isSlowDecision(ssn){
    return ssn === "111111111";
}