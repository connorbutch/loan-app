const AWSXRay = require('aws-xray-sdk');
AWSXRay.setContextMissingStrategy(() => {
});

const metricNamespace = "loan";
const { createMetricsLogger } = require("aws-embedded-metrics");

exports.handler = async (event, context) => {
    return await AWSXRay.captureAsyncFunc('handleRequest',
        async function (subsegment) {
            console.log(event);
            for (const loanApprovedEvent of event.Records.map(record => record.body)
                .map(bodyString => JSON.parse(bodyString))) {
                await publishLoanApprovedMetric(loanApprovedEvent);
            }
            subsegment.close();
            return {};
        });
};

async function publishLoanApprovedMetric(loanApprovedEvent) {
        const loanApplicationId = loanApprovedEvent.loanApplicationId;
        const metrics = createMetricsLogger();
        metrics.setNamespace(metricNamespace);
        metrics.putMetric("loanApprovals", 1);
        metrics.setProperty("loanApplicationId", loanApplicationId);
        await metrics.flush();
}