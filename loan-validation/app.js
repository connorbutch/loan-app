const {SNSClient} = require("@aws-sdk/client-sns");
const {PublishCommand} = require("@aws-sdk/client-sns");
const AWSXRay = require('aws-xray-sdk');
AWSXRay.setContextMissingStrategy(() => {
});

const topicArn = process.env.TOPIC_ARN;
const snsClient = AWSXRay.captureAWSv3Client(new SNSClient({region: process.env.AWS_REGION}));

exports.handler = async (event, context) => {
    return await AWSXRay.captureAsyncFunc('handleRequest',
        async function () {
            recordRequestMetadata(event);
            let response;
            const errors = AWSXRay.captureFunc('validateRequest', function () {
                return validateRequest(event);
            });
            if (errors != null) {
                AWSXRay.getSegment().addAnnotation("isValidRequest", false);
                response = buildErrorResponse(errors);
            } else {
                const loanApplicationId = getLoanApplicationId(event);
                AWSXRay.getSegment().addAnnotation("isValidRequest", true);
                response = await AWSXRay.captureAsyncFunc('handleValidRequest', async function (nestedSubsegment) {
                    return await handleValidRequest(event, loanApplicationId, nestedSubsegment);
                })
            }
            const requestId = new Date().getTime(); //TODO generate uuid in the future, this is good for now
            response.headers["x-request-id"] = requestId;
            AWSXRay.getSegment().addAnnotation("requestId", requestId);
            recordResponseMetaDataAndClose(response);
            return response;
        });
};

async function handleValidRequest(event, loanApplicationId, subsegment) {
    let response;
    console.log(event.body);
    const body = JSON.parse(event.body);
    body.loanApplicationId = loanApplicationId;
    try {
        if (body.ssn === "222222222") {
            throw new Error('Oh no, error');
        }
        const publishCommand = new PublishCommand({
            Message: JSON.stringify(body),
            TopicArn: topicArn,
            MessageAttributes: {
                "TYPE": {
                    "DataType": "String",
                    "StringValue": "LOAN_REQUESTED",
                    "BinaryValue": null
                }
            }
        });
        subsegment.addMetadata("publish_command", publishCommand.input, "sns");
        const result = await snsClient.send(publishCommand);
        subsegment.addMetadata("publish_output", result, "sns");
        subsegment.addAnnotation('loanApplicationId', loanApplicationId);
        response = {
            'statusCode': 202,
            'body': JSON.stringify(body),
            'headers': {
                'Content-type': 'application/json'
            }
        };
        subsegment.close();
    } catch (err) {
        console.log('got error: ', err);
        response = {
            'statusCode': 500,
            'body': JSON.stringify({
                message: 'error'
            }),
            'headers': {
                'Content-type': 'application/json'
            }
        };
        subsegment.addError(new Error(err));
        subsegment.close(err);
    }
    return response;
}

function buildErrorResponse(errors) {
    //TODO in the real world we would do this, but ommitting for brevity in the demo
}

function validateRequest(event) {
    //TODO in the real world we would do this, but ommitting for brevity in the demo
    console.warn("TODO not yet validating request");
    return null;
}

function getLoanApplicationId(event) {
    return event.pathParameters?.loanApplicationId; //NOTE: should never be null as we validate it first
}

function recordRequestMetadata(event) {
    AWSXRay.getSegment().addMetadata("request_body", event.body, "request"); //TODO be careful with this in real-life not to leak confidential information.  This is just here for demo purposes
    AWSXRay.getSegment().addMetadata("request_path", event.rawPath, "request"); //TODO be careful with this in real-life not to leak confidential information.  This is just here for demo purposes
    AWSXRay.getSegment().addMetadata("request_headers", event.headers, "request"); //TODO be careful with this in real-life not to leak confidential information.  This is just here for demo purposes
}

function recordResponseMetaDataAndClose(response) {
    AWSXRay.getSegment().addMetadata("response", response, "response")
    AWSXRay.getSegment().close();
}