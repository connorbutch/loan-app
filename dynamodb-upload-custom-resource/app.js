const AWS = require("aws-sdk");
AWS.config.update({region: process.env.AWS_REGION})
const dynamoDB = new AWS.DynamoDB.DocumentClient();

const https = require("https");
const url = require("url");

exports.handler = async function (event, context) {
    console.log("REQUEST RECEIVED:\n" + JSON.stringify(event));
    //NOTE: in a real scenario, you should always be SUPER CAREFUL with custom resources, and surround with try catch
    let responseData = {};
    try {
        if (event.RequestType === "Create" || event.RequestType === "Update") {
            responseData = await handleUpsert(event);
        } else if (event.RequestType === "Delete") {
            //here is where you would handle deletion, but this is on x-ray, not custom resources
            responseData = {
                "yeahWeDontHandleDeletes": "randomDataYeah!"
            };
        }
    }catch (err){
        console.error(err);
    }
    return await sendResponse(event, context, "SUCCESS", responseData);
};

async function handleUpsert(event){
    const tableName = event.ResourceProperties.TableName;
    console.log("table name: ", tableName);
    const dataToInsertRaw = event.ResourceProperties.DataToInsert;
    console.log("data raw: ", dataToInsertRaw);
    const dataNoParse = event.ResourceProperties.DataToInsert.Data;
    console.log("data no parse: ", dataNoParse);
    const putItems = dataNoParse.map(data => {
        console.log("individual data:", JSON.stringify(data));
        return  {
            PutRequest: {
                Item: data
            }
        };
    })
    const batchWriteItemInput = {
        RequestItems: {

        }
    };
    batchWriteItemInput.RequestItems[tableName] = putItems;
    console.log(JSON.stringify(batchWriteItemInput));
    try{
        //TODO save data to given table
       const batchWriteOutput = await dynamoDB.batchWrite(batchWriteItemInput).promise();
       console.log(JSON.stringify(batchWriteOutput));
    }catch(err){
        console.error(err);
    }
    return {
        "youCouldUseThisWithGetAttInCF": "arbitraryValue"
    }
}

async function sendResponse(event, context, responseStatus, responseData) {
    let responsePromise = new Promise((resolve, reject) => {
        const responseBody = JSON.stringify({
            Status: responseStatus,
            Reason:
                "See the details in CloudWatch Log Stream: " + context.logStreamName,
            PhysicalResourceId: context.logStreamName,
            StackId: event.StackId,
            RequestId: event.RequestId,
            LogicalResourceId: event.LogicalResourceId,
            Data: responseData,
        });
        console.log("RESPONSE BODY:\n", responseBody);
        const parsedUrl = url.parse(event.ResponseURL);
        const options = {
            hostname: parsedUrl.hostname,
            port: 443,
            path: parsedUrl.path,
            method: "PUT",
            headers: {
                "content-type": "",
                "content-length": responseBody.length,
            },
        };
        console.log("SENDING RESPONSE...\n");
        const request = https.request(options, function (response) {
            console.log("STATUS: " + response.statusCode);
            console.log("HEADERS: " + JSON.stringify(response.headers));
            // Tell AWS Lambda that the function execution is done
            resolve(JSON.parse(responseBody));
            context.done();
        });
        request.on("error", function (error) {
            console.log("sendResponse Error:" + error);
            // Tell AWS Lambda that the function execution is done
            reject(error);
            context.done();
        });

        // write data to request body
        request.write(responseBody);
        request.end();
    });
    return await responsePromise;

}
